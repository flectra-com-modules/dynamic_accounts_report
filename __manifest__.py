#############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#
#    Copyright (C) 2021-TODAY Cybrosys Technologies(<https://gitlab.com/flectra-community/dynamic_accounts_report-flectra>)
#    Author: Cybrosys Techno Solutions, Jamotion GmbH(<https://gitlab.com/flectra-community/dynamic_accounts_report-flectra>)
#
#    You can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################

{
    'name': 'Dynamic Financial Reports',
    'version': '2.0.1.2.7',
    'category': 'Accounting',
    'live_test_url': 'https://www.youtube.com/watch?v=gVQi9q9Rs-E&t=5s',
    'summary': """Dynamic Financial Reports with drill 
                down and filters– Community Edition""",
    'description': "Dynamic Financial Reports, DynamicFinancialReports, FinancialReport, Accountingreports, flectra reports, flectra"
                   "This module creates dynamic Accounting General Ledger, Trial Balance, Balance Sheet "
                   "Proft and Loss, Cash Flow Statements, Partner Ledger,"
                   "Partner Ageing, Day book"
                   "Bank book and Cash book reports in Odoo 14 community edition.",
    'author': 'Cybrosys Techno Solutions, SZSolutions',
    'website': "https://gitlab.com/flectra-community/dynamic_accounts_report-flectra",
    'company': 'Cybrosys Techno Solutions, SZSolutions',
    'maintainer': 'Cybrosys Techno Solutions, SZSolutions',
    'depends': ['base','account'],
    'data': [
        'security/ir.model.access.csv',
        'views/templates.xml',
        'views/views.xml',
        'report/trial_balance.xml',
        'report/general_ledger.xml',
        'report/cash_flow_report.xml',
        'report/financial_report_template.xml',
        'report/partner_ledger.xml',
        'report/ageing.xml',
        'report/daybook.xml',
    ],
    'qweb': [
        'static/src/xml/general_ledger_view.xml',
        'static/src/xml/trial_balance_view.xml',
        'static/src/xml/cash_flow_view.xml',
        'static/src/xml/financial_reports_view.xml',
        'static/src/xml/partner_ledger_view.xml',
        'static/src/xml/ageing.xml',
        'static/src/xml/daybook.xml',
    ],
    'license': 'LGPL-3',
    'images': ['static/description/banner.png'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
